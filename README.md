GJEL Accident Attorneys is a personal injury law firm that specializes in helping injured victims get the compensation they deserve. GJEL handles serious injury and wrongful death matters only. For over the past 40 years we have helped thousands of clients and have maintained a 99% success rate with our cases.

Address: 1625 The Alameda, #511, San Jose, CA 95126

Phone: 408-955-9000
